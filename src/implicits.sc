/* Scala Implicit Conversions
 * --------------------------
 *
 *
 *
 *
 */

// The following example shows scala creating a Range object directly
// from two Ints, and a method called 'to'.

val a: Int = 1
val b: Int = 4
val myRange: Range = a to b

// 'to' is simply a method defined in the RichInt class (provided with scala),
// which takes a parameter and returns a Range object. You could rewrite it as
// the following if you really wanted to:

val myRangeB : Range = a.to(b)

// Bit how is the 'to' method from RichInt made available to a normal Int class?
// This is where the implicit conversion comes into play. Scala is automatically
// converting the Int '1' to a RichInt (see Predef.scala)

// As an example lets create a new class which has a x10 function

class SuperInt(value: Int) {
  def timesTen = value*10
}

// Next, lets define an implicit conversion from a String to our new class (the name can be anything)

implicit def String2SuperInt(value: String) = new SuperInt(Integer.parseInt(value))

// We can now use our x10 function on strings!

"6" timesTen
